menuf = love.graphics.newFont("graphics/fonts/menu.ttf", 22)
love.graphics.setFont( menuf )

player = {}

player.texture = 48

if love.filesystem.isFile("player.mise") then
	player.mise = love.filesystem.read("player.mise")
	player.chapter = love.filesystem.read("player.chapter")
	player.points = love.filesystem.read("player.points")
else
	player.mise = 1
	player.chapter = 0
	player.points = 0
	love.filesystem.newFile("player.chapter")
	love.filesystem.write("player.chapter", player.chapter)
	love.filesystem.newFile("player.mise")
	love.filesystem.write("player.mise", player.mise)
	love.filesystem.newFile("player.points")
	love.filesystem.write("player.points", player.points)
end

if love.filesystem.isFile("player.max") then
	tmp = love.filesystem.read("player.max")
	player.max = tonumber(tmp)
else
	player.max = 5
	love.filesystem.newFile("player.max")
	love.filesystem.write("player.max", player.max)
end

gamestate = {require("menu"), require("mise"), require("kolo"), require("extra"), require("setting"), require("quit")}
language = require("text/czech")
state = 1

--ziskani dlc listu

http = require("socket.http")
local b, c, h = http.request("http://openfun.eu/plosinovka/dlc/list")
love.filesystem.write("dlc_list", b)

--audio stopy

music = {love.audio.newSource("audio/menu.ogg", "stream"),
         love.audio.newSource("audio/choose.ogg", "stream"),
         love.audio.newSource("audio/game.ogg", "stream")}

music[1]:setLooping(true)
music[2]:setLooping(true)
music[3]:setLooping(true)

effect = {
	jump = love.audio.newSource("audio/jump.ogg", "static"),
	lose = love.audio.newSource("audio/lose.ogg", "static"),
	pick = love.audio.newSource("audio/pick.ogg", "static"),
	pick2 = love.audio.newSource("audio/pick2.ogg", "static"),
	win = love.audio.newSource("audio/win.ogg", "static")
	}

--obecne funkce

function round(num)
    under = math.floor(num)
    upper = math.floor(num) + 1
    underV = -(under - num)
    upperV = upper - num
    if (upperV > underV) then
        return under
    else
        return upper
    end
end

--funkce konec

function love.load()
  if love.system.getOS() ~= "Android" then
	  print("\n---- SUPPORTED ---- ");
	  print("Canvas:         " .. tostring(love.graphics.getSupported('canvas')));
	  print("PO2:            " .. tostring(love.graphics.getSupported('npot')));
	  print("Subtractive BM: " .. tostring(love.graphics.getSupported('subtractive')));
	  print("Shaders:        " .. tostring(love.graphics.getSupported('shader')));
	  print("HDR Canvas:     " .. tostring(love.graphics.getSupported('hdrcanvas')));
	  print("Multicanvas:    " .. tostring(love.graphics.getSupported('multicanvas')));
	  print("Mipmaps:        " .. tostring(love.graphics.getSupported('mipmap')));
	  print("DXT:            " .. tostring(love.graphics.getSupported('dxt')));
	  print("BC5:            " .. tostring(love.graphics.getSupported('bc5')));
	  print("SRGB:           " .. tostring(love.graphics.getSupported('srgb')));

	  print("\n---- RENDERER  ---- ");
	  local name, version, vendor, device = love.graphics.getRendererInfo()
	  print(string.format("Name: %s \nVersion: %s \nVendor: %s \nDevice: %s", name, version, vendor, device));
  end

  update = true
  min_dt = 1/60
  next_time = love.timer.getTime()
  if gamestate[state].load then gamestate[state].load() end
end

function love.update(dt)
  --audio handle
  if state ~= 2 and state ~= 3 then
    music[1]:play()
    music[2]:stop()
    music[3]:stop()
  elseif state == 2 then
    music[1]:stop()
    music[2]:play()
    music[3]:stop()
  elseif state == 3 then
    music[1]:stop()
    music[2]:stop()
    music[3]:play()
  end
  --end
  if update then
	tlacitka.state = state
  end
  if update and state ~= 3 then
	menuf = love.graphics.newFont("graphics/fonts/menu.ttf", player.texture/2.5)
	love.graphics.setFont( menuf )
  end
  mousex = love.mouse.getX()
  mousey = love.mouse.getY()
  width = love.graphics.getWidth( )
  height = love.graphics.getHeight( )
  next_time = next_time + min_dt
  if gamestate[state].update then gamestate[state].update(dt) end
end

function love.draw()

  if gamestate[state].draw then gamestate[state].draw() end
  --love.graphics.print("Current FPS: "..tostring(love.timer.getFPS( )), 10, height-40)
  local cur_time = love.timer.getTime()
  if next_time <= cur_time then
    next_time = cur_time
    return
  end
  love.timer.sleep(next_time - cur_time)
end

function love.mousepressed(x, y, button)
  if gamestate[state].mousepressed then gamestate[state].mousepressed(x, y, button) end
end

function love.keypressed(key)
  if key == "f11" then
  toggle = not toggle
	love.window.setFullscreen(toggle, "desktop")
  end
  if gamestate[state].keypressed then gamestate[state].keypressed(key) end
end

function love.textinput(t)
  if gamestate[state].textinput then gamestate[state].textinput(t) end
end
