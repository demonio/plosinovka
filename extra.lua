local extra = {}
local tlacitka = require("buttons")
local bg = love.graphics.newImage("graphics/bg/darkPurple.png")
bg:setWrap("repeat", "repeat")

dlc_name = {}
dlc_url = {}
local_dlc = "none"
folder = 6
first = true
download = false

function extra.update()
    if first then
      if love.filesystem.isFile("dlc_list") then
        if love.filesystem.isFile("local.dlc") then
              local_dlc = love.filesystem.read("local.dlc")
        end
        for line in love.filesystem.lines("dlc_list") do
            i, k = line:match("^(%S*) (%S*)")
            table.insert(dlc_name, i)
            table.insert(dlc_url, k)
        end
      end
    first = false
    end
    quad = love.graphics.newQuad(0, 0, width, height, 256, 256)
    if update then
        tlacitka.clear()
        for key, value in pairs(dlc_name) do
            if not string.match(local_dlc, value) then
                tlacitka.spawn(width / 2 - 100, height / 2 - (100-(key*50)), value, key)
            end
        end
        tlacitka.spawn(width / 2 - 100, height / 2 + 300, language.back, "back")
        update = false
    end
    tlacitka.check()
    local press = tlacitka.presset()
    if press == "back" then
        state = 1
        return
    end
    if press ~= nil then
          extra.buy(dlc_url[press], dlc_name[press])
    end
end

function extra.buy(url, name)
    if tonumber(player.points) > 299 then
        player.points = tonumber(player.points) - 300
        love.filesystem.write("player.points", player.points)
    else
        return
    end
    while love.filesystem.isFile("maps/"..folder) do
        folder = folder + 1
    end
    love.filesystem.createDirectory("maps/"..folder)
    for i=1,5,1 do
        local b, c, h = http.request(url..i..".lua")
        love.filesystem.write("maps/"..folder.."/"..i..".lua", b)
    end
    local b, c, h = http.request(url.."bg.png")
    love.filesystem.write("maps/"..folder.."/bg.png", b)
    local b, c, h = http.request(url.."images.png")
    love.filesystem.write("maps/"..folder.."/images.png", b)
    print("Dlc download done")
    download = true
    if not love.filesystem.isFile("local.dlc") then
        love.filesystem.newFile("local.dlc")
    end
    player.max = player.max + 1
	love.filesystem.write("player.max", player.max)
    love.filesystem.append("local.dlc", name)
end

function extra.mousepressed(x, y, button)
  if button == "l" then
      tlacitka.click(x, y)
  end
end

function extra.keypressed(key)
  if key and download then
      download = false
      first = true
      dlc_name = {}
      dlc_url = {}
      local_dlc = "none"
      folder = 6
  end
  tlacitka.keyboard(key)
end

function extra.draw()
  love.graphics.draw(bg, quad, 0, 0)
  love.graphics.setColor(0, 0, 0)
  love.graphics.print(language.dlc_buy, width/2-menuf:getWidth(language.dlc_buy)/2, 35)
  if download then
      love.graphics.print(language.download, width/2-menuf:getWidth(language.download)/2, 50)
  end
  love.graphics.print(language.points..player.points, (width-menuf:getWidth(language.points..tostring(player.points))) - 10, 10)
  love.graphics.setColor(255, 255, 255)
  tlacitka.draw()
end

return extra
