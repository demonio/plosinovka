return {
  version = "1.1",
  luaversion = "5.1",
  orientation = "orthogonal",
  width = 40,
  height = 20,
  tilewidth = 70,
  tileheight = 70,
  properties = {},
  tilesets = {
    {
      name = "images",
      firstgid = 1,
      tilewidth = 70,
      tileheight = 70,
      spacing = 0,
      margin = 0,
      image = "../0/images.png",
      imagewidth = 630,
      imageheight = 630,
      properties = {},
      tiles = {
        {
          id = 0,
          properties = {
            ["data"] = "box"
          }
        },
        {
          id = 1,
          properties = {
            ["data"] = "box"
          }
        },
        {
          id = 6,
          properties = {
            ["data"] = "box"
          }
        },
        {
          id = 15,
          properties = {
            ["data"] = "ground"
          }
        },
        {
          id = 16,
          properties = {
            ["data"] = "most"
          }
        },
        {
          id = 17,
          properties = {
            ["data"] = "most"
          }
        },
        {
          id = 27,
          properties = {
            ["data"] = "lader"
          }
        },
        {
          id = 28,
          properties = {
            ["data"] = "coin"
          }
        },
        {
          id = 29,
          properties = {
            ["data"] = "start"
          }
        },
        {
          id = 31,
          properties = {
            ["data"] = "end"
          }
        },
        {
          id = 35,
          properties = {
            ["data"] = "krystal"
          }
        },
        {
          id = 36,
          properties = {
            ["data"] = "krystal"
          }
        },
        {
          id = 37,
          properties = {
            ["data"] = "krystal"
          }
        },
        {
          id = 38,
          properties = {
            ["data"] = "krystal"
          }
        },
        {
          id = 39,
          properties = {
            ["data"] = "ground"
          }
        },
        {
          id = 40,
          properties = {
            ["data"] = "ground"
          }
        },
        {
          id = 41,
          properties = {
            ["data"] = "ground"
          }
        },
        {
          id = 42,
          properties = {
            ["data"] = "ground"
          }
        },
        {
          id = 43,
          properties = {
            ["data"] = "ground"
          }
        },
        {
          id = 44,
          properties = {
            ["data"] = "ground"
          }
        },
        {
          id = 45,
          properties = {
            ["data"] = "ground"
          }
        },
        {
          id = 46,
          properties = {
            ["data"] = "ground"
          }
        },
        {
          id = 47,
          properties = {
            ["data"] = "ground"
          }
        },
        {
          id = 48,
          properties = {
            ["data"] = "ground"
          }
        },
        {
          id = 49,
          properties = {
            ["data"] = "ground"
          }
        },
        {
          id = 50,
          properties = {
            ["data"] = "ground"
          }
        },
        {
          id = 51,
          properties = {
            ["data"] = "ground"
          }
        },
        {
          id = 52,
          properties = {
            ["data"] = "ground"
          }
        },
        {
          id = 53,
          properties = {
            ["data"] = "ground"
          }
        },
        {
          id = 54,
          properties = {
            ["data"] = "ground"
          }
        },
        {
          id = 55,
          properties = {
            ["data"] = "ground"
          }
        },
        {
          id = 56,
          properties = {
            ["data"] = "ground"
          }
        },
        {
          id = 57,
          properties = {
            ["color"] = "blue",
            ["data"] = "key"
          }
        },
        {
          id = 58,
          properties = {
            ["color"] = "green",
            ["data"] = "key"
          }
        },
        {
          id = 59,
          properties = {
            ["color"] = "orange",
            ["data"] = "key"
          }
        },
        {
          id = 60,
          properties = {
            ["color"] = "yellow",
            ["data"] = "key"
          }
        },
        {
          id = 61,
          properties = {
            ["data"] = "lader"
          }
        },
        {
          id = 62,
          properties = {
            ["data"] = "lader"
          }
        },
        {
          id = 63,
          properties = {
            ["data"] = "spikes"
          }
        },
        {
          id = 64,
          properties = {
            ["data"] = "spikes"
          }
        },
        {
          id = 65,
          properties = {
            ["data"] = "spikes"
          }
        },
        {
          id = 66,
          properties = {
            ["color"] = "blue",
            ["data"] = "lock"
          }
        },
        {
          id = 67,
          properties = {
            ["color"] = "green",
            ["data"] = "lock"
          }
        },
        {
          id = 68,
          properties = {
            ["color"] = "orange",
            ["data"] = "lock"
          }
        },
        {
          id = 69,
          properties = {
            ["color"] = "yellow",
            ["data"] = "lock"
          }
        },
        {
          id = 74,
          properties = {
            ["data"] = "spikes"
          }
        },
        {
          id = 75,
          properties = {
            ["data"] = "jump"
          }
        },
        {
          id = 76,
          properties = {
            ["data"] = "jump"
          }
        },
        {
          id = 77,
          properties = {
            ["data"] = "hvezda"
          }
        },
        {
          id = 78,
          properties = {
            ["data"] = "paka",
            ["hodnota"] = "-1"
          }
        },
        {
          id = 79,
          properties = {
            ["data"] = "paka",
            ["hodnota"] = "0"
          }
        },
        {
          id = 80,
          properties = {
            ["data"] = "paka",
            ["hodnota"] = "1"
          }
        }
      }
    }
  },
  layers = {
    {
      type = "tilelayer",
      name = "background",
      x = 0,
      y = 0,
      width = 40,
      height = 20,
      visible = true,
      opacity = 1,
      properties = {},
      encoding = "lua",
      data = {
        0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
        73, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 73, 0,
        73, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
        0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 19, 0, 0, 0, 0, 0, 0,
        0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
        0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 73, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
        0, 0, 0, 0, 0, 0, 0, 0, 73, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
        0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
        0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 19, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
        0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
        0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
        0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
        0, 0, 0, 0, 0, 0, 19, 19, 19, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
        0, 0, 0, 0, 0, 0, 19, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 73,
        0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
        0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
        0, 0, 0, 0, 0, 0, 73, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 73, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
        0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
        0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
        0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0
      }
    },
    {
      type = "tilelayer",
      name = "mapa24",
      x = 0,
      y = 0,
      width = 40,
      height = 20,
      visible = true,
      opacity = 1,
      properties = {},
      encoding = "lua",
      data = {
        0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 29, 0, 0,
        58, 0, 0, 0, 0, 0, 29, 0, 0, 0, 0, 0, 0, 29, 0, 0, 0, 0, 0, 0, 29, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
        56, 0, 0, 0, 0, 0, 0, 0, 0, 0, 47, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 47, 0, 0, 0, 0, 0, 0, 0, 0, 0, 29, 0, 0, 0, 48, 48, 49, 50,
        0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 47, 0, 0, 0, 0, 0, 0, 47, 47, 0, 0, 0, 0, 29, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
        0, 0, 0, 0, 0, 56, 57, 0, 0, 0, 0, 0, 0, 0, 0, 29, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 55, 0, 0, 56, 57, 0, 0, 0, 0, 0, 0,
        0, 0, 0, 0, 0, 0, 0, 0, 0, 29, 0, 0, 0, 0, 0, 0, 47, 0, 0, 0, 0, 0, 29, 0, 0, 0, 47, 0, 0, 0, 0, 0, 0, 0, 0, 47, 0, 0, 0, 0,
        0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 47, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 47, 0,
        0, 0, 29, 0, 0, 0, 55, 55, 55, 56, 57, 0, 0, 0, 0, 0, 0, 0, 0, 29, 0, 0, 0, 47, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 29, 0, 0, 0,
        0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 29, 0, 0, 0, 29, 0, 0, 0, 0, 0, 0, 0, 0, 0,
        0, 0, 0, 0, 47, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 55, 56, 57, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 47, 0, 0, 0, 55, 55, 0, 57,
        47, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 47, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
        0, 0, 0, 0, 0, 0, 29, 0, 0, 0, 0, 0, 29, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
        0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 47, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
        0, 0, 0, 55, 55, 55, 55, 56, 57, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 55, 55, 0, 0, 0, 0, 29, 0, 0, 47, 0, 0, 0, 0, 0,
        0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 40, 40, 40,
        31, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 56, 57, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 47, 0, 0, 0, 0, 41, 0, 33,
        30, 0, 0, 0, 0, 0, 0, 0, 0, 47, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 39, 0, 0, 0, 0, 0, 0, 0, 0, 47, 47, 0, 0, 0, 0, 0, 67, 0, 32,
        48, 49, 50, 50, 0, 0, 47, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 40, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 55, 55, 56, 57, 57,
        66, 66, 66, 66, 66, 66, 66, 66, 66, 66, 66, 66, 66, 66, 66, 66, 66, 66, 66, 66, 66, 66, 66, 66, 66, 66, 66, 66, 66, 66, 66, 66, 66, 66, 66, 66, 66, 66, 66, 66,
        64, 64, 64, 64, 64, 64, 64, 64, 64, 64, 64, 64, 64, 64, 64, 64, 64, 64, 64, 64, 64, 64, 64, 64, 64, 64, 64, 64, 64, 64, 64, 64, 64, 64, 64, 64, 64, 64, 64, 64
      }
    }
  }
}
