local setting = {}
local click = nil
tlacitka = require("buttons")

local toggle = false

bg = love.graphics.newImage("graphics/bg/darkPurple.png")
bg:setWrap("repeat", "repeat")

function screen_toggle()
  toggle = not toggle
	love.window.setFullscreen(toggle, "desktop")
  click = nil
end

function setting.update(dt)
  quad = love.graphics.newQuad(0, 0, width, height, 256, 256)
  if update then
      tlacitka.clear()
      tlacitka.spawn(width/2 - (10+(200*tlacitka.scale)/2), height/2 -((50*tlacitka.scale)*3), "48", 48)
      tlacitka.spawn(width/2 - (10+(200*tlacitka.scale)/2), height/2 -((50*tlacitka.scale)*2), "70", 70)
      tlacitka.spawn(width/2 - (10+(200*tlacitka.scale)/2), height/2 -((50*tlacitka.scale)), "96", 96)
      tlacitka.spawn(width/2 - (10+(200*tlacitka.scale)/2), height/2, "128", 128)
      tlacitka.spawn(width/2 - (10+(200*tlacitka.scale)/2), height/2 +((50*tlacitka.scale)), language.fullscreen, "screen")
      tlacitka.spawn(width/2 - (10+(200*tlacitka.scale)/2), height/2 +((50*tlacitka.scale)*2), language.back, "back")
      update = false
  end
  tlacitka.check()
  click = tlacitka.presset()
  if click == "screen" then
      screen_toggle()
      return
  end
  if click == "back" then
      state = 1
      return
  end
  if click ~= nil then
      player.texture = click
  end
end

function setting.draw()
  love.graphics.draw(bg, quad, 0, 0)
  tlacitka.draw()
end

function setting.mousepressed(x, y, button)
  if button == "l" then
    tlacitka.click(x, y)
  end
end

function setting.keypressed(key)
  tlacitka.keyboard(key)
end


return setting
