local mission = {}

local tlacitka = require("buttons")

local bg = love.graphics.newImage("graphics/bg/darkPurple.png")
bg:setWrap("repeat", "repeat")

chapter = nil
mise = nil

timer = 0

function mission.update(dt)
  chapter_default = player.chapter
  mise_default = player.mise
  if chyba then
     if timer > 249 then
       chyba = nil
       timer = 0
     end
    timer = timer + 1
  end
  quad = love.graphics.newQuad(0, 0, width, height, 256, 256)
  if update then
      if mise == nil then
        tlacitka.clear()
        if chapter == nil then
          for i=0,player.max do
            if tonumber(chapter_default) > i then
              hotovo = true
            else
              hotovo = nil
            end
            tlacitka.spawn(width/2 - (10+(200*tlacitka.scale)/2),
                height/2 +((50*tlacitka.scale)*(i-((player.max/2)+1))), language.chapter .. i, i, hotovo)
          end
        else
          for i=1,5 do
            if tonumber(chapter_default) > chapter or tonumber(mise_default) > i then
              hotovo = true
            else
              hotovo = nil
            end
            tlacitka.spawn(width/2 - (10+(200*tlacitka.scale)/2),
                height/2 +((50*tlacitka.scale)*(i-3)), language.mission .. i, i, hotovo)
          end
        end

      else
        tlacitka.clear()
      end
      tlacitka.spawn(width/2 - (10+(200*tlacitka.scale)/2), height -(10+(50*tlacitka.scale)), language.back, "back")
      update = false
  end
  tlacitka.check()
  local press = tlacitka.presset()
  if press == "back" then
      if chapter == nil then
        tlacitka.keyboard("escape")
      else
        chapter = nil
        update = true
      end
      return
  end
  if press ~= nil then
    if chapter == nil then
      if tonumber(chapter_default) >= press then
        chapter = press
      else
        chyba = language.chapter_low
      end
    else
      if tonumber(mise_default) >= press or tonumber(chapter_default) > chapter then
          mise = press
      gamestate[3].load()
          state = 3
      else
        chyba = language.mise_low
      end
    end
  end
end

function mission.draw()
  love.graphics.draw(bg, quad, 0, 0)
  if mise == nil then
    tlacitka.draw()
  end
  if chyba then
    love.graphics.setColor(255,0,0)
    love.graphics.print(chyba, width/2 - menuf:getWidth(chyba)/2, height/2)
    love.graphics.setColor(255,255,255)
  end
end

function mission.keypressed(key)
  if mise == nil then
    if key == "escape" then
      if chapter == nil then
        tlacitka.keyboard(key)
      else
        chapter = nil
        update = true
      end
    else
      tlacitka.keyboard(key)
    end
  end
end

function mission.mousepressed(x, y, button)
  if button == "l" then
      tlacitka.click(x,y)
  end
end

return mission
