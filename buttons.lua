-- Proměne ktere potřebujeme na generovaní tlačítek
local tlacitka = {}
local button = {}
local buttonbg = love.graphics.newImage("graphics/images/ui/button_bg.png")
local buttonbg_active = love.graphics.newImage("graphics/images/ui/button_bg_active.png")
local buttonbg_back = love.graphics.newImage("graphics/images/ui/button_bg.png")
local buttonbg_active_back = buttonbg_active
local button_blank = love.graphics.newImage("graphics/images/ui/blank.png")
local game_button = {}
game_button["left"] = love.graphics.newImage("graphics/images/ui/left.png")
game_button.right = love.graphics.newImage("graphics/images/ui/right.png")
game_button.up = love.graphics.newImage("graphics/images/ui/up.png")
game_button.down = love.graphics.newImage("graphics/images/ui/down.png")
game_button.jump = love.graphics.newImage("graphics/images/ui/jump.png")
local medal = love.graphics.newImage("graphics/medals/1.png")
local buttonbg_x = (buttonbg:getWidth( ) / 2) - 2
local buttonbg_y = (buttonbg:getHeight( ) / 2) - 2
tlacitka.scale= player.texture/48
tlacitka.timer = 15
tlacitka.state = 1

-- Vytvoření tlačítka
function tlacitka.spawn(x, y, text, value, hotovo)
	table.insert(button, {x = x, y = y, text = text, mouseover = false, value = value, hotovo = hotovo, presset = false})
end

-- Vykreslení tlačítka
function tlacitka.draw()
	buttonbg_x = ((buttonbg:getWidth( ) / 2) - 2)*tlacitka.scale
	buttonbg_y = ((buttonbg:getHeight( ) / 2) - 2)*tlacitka.scale
	for i, v in ipairs(button) do
		if v.mouseover == true then
			love.graphics.draw(buttonbg_active, v.x, v.y, 0, tlacitka.scale, tlacitka.scale)

		else
			love.graphics.draw(buttonbg, v.x, v.y, 0, tlacitka.scale, tlacitka.scale)

		end
    if v.hotovo then
      love.graphics.draw(medal, v.x + ((buttonbg_x)*2)+15, v.y, 0, tlacitka.scale, tlacitka.scale)
      love.graphics.draw(medal, v.x - ((medal:getWidth()*tlacitka.scale)+10), v.y, 0, tlacitka.scale, tlacitka.scale)
      love.graphics.setColor(0, 255, 0)
    end
		if tlacitka.state ~= 3 then
			love.graphics.print( v.text, v.x + (buttonbg_x - menuf:getWidth(v.text)/2),
			 	v.y + (buttonbg_y - menuf:getHeight(v.text)/2))
		else
			love.graphics.draw(game_button[v.value], v.x, v.y, 0, tlacitka.scale*2, tlacitka.scale*2)
		end
		love.graphics.setColor(255, 255, 255)
	end
end

-- Testovaní jestli je tlačítko zmačknuté
function tlacitka.click(x, y)
	for i, v in ipairs(button) do
		if x > v.x and x < v.x + (buttonbg_x * 2) * tlacitka.scale
		and y > v.y and y < v.y + (buttonbg_y * 2) * tlacitka.scale then
			v.presset = true
		end
	end
end

function tlacitka.presset()
	for i, v in ipairs(button) do
		if v.presset then
			v.presset = false
			update = true
			return v.value
		end
	end
	return nil
end

function tlacitka.pres_table()
	tmp = {}
	for i, v in ipairs(button) do
		if v.presset then
			v.presset = false
			update = true
			table.insert(tmp, v.value)
		end
	end
	return tmp
end

-- Testovaní najetí myší
function tlacitka.check()
	if love.system.getOS() == "Android" then
		if tlacitka.timer == 0  or state == 3 then
			local touches = love.touch.getTouches()

			for i, id in ipairs(touches) do
				local mousex, mousey = love.touch.getPosition(id)
				for i, v in ipairs(button) do
					if mousex > v.x and mousex < v.x + (buttonbg_x * 2)
					and mousey > v.y and mousey < v.y + (buttonbg_y * 2) then
						v.presset = true
						tlacitka.timer = 15
					end
				end
			end
		else
			tlacitka.timer = tlacitka.timer-1
		end

	else
		local tmp = 0
		for i, v in ipairs(button) do
			if mousex > v.x and mousex < v.x + (buttonbg_x * 2)
			and mousey > v.y and mousey < v.y + (buttonbg_y * 2) then
				v.mouseover = true
				tmp = i
			end
		end
		if tmp > 0 then
			for i, v in ipairs(button) do
				if v.mouseover == true and i ~= tmp then
					v.mouseover = false
				end
			end
		end
	end
end

function tlacitka.keyboard(key)
	if key == "escape" then
		if state == 1 then
			love.event.quit()
			update = true
		elseif state ~= 3 then
			state = 1
			update = true
		else
			state = 2
			update = true
		end
	end
	if key == "down" then
		local check = false
		for i, v in ipairs(button) do
			if check == true then
				v.mouseover = true
				return
			end
			if v.mouseover == true then
				check = true
				v.mouseover = false
			end
		end
		if not check then
			for i, v in ipairs(button) do
				v.mouseover = true
				return
			end
		end
	end
	if key == "up" then
		local check = false
		local tmp = 0
		for i, v in ipairs(button) do
			if v.mouseover == true then
				check = true
				v.mouseover = false
				tmp = i - 1
			end
		end
		if tmp > 0 then
			button[tmp].mouseover = true
		end
		if not check then
			button[table.getn(button)].mouseover = true
		end
	end
	if key == "return" then
		for i, v in ipairs(button) do
			if v.mouseover == true then
				print(i)
				v.presset = true
			end
		end
	end
end

-- Smazaní všech hlavních tlačítek
function tlacitka.clear()
	if tlacitka.state == 3 and update then
		buttonbg = button_blank
		tlacitka.scale= player.texture/96
	elseif update then
		buttonbg = buttonbg_back
		tlacitka.scale= player.texture/48
	end
	for k in pairs (button) do
		button [k] = nil
	end
end

return tlacitka
