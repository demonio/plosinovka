local kolo = {}

local tlacitka = require("buttons")

segment = {}
keys = {}
level = {}
level_bg = {}
texture = player.texture
map_x = 0
map_y = 0
properties = {}
procolor = {}
map_color = {}
player.key = {}
player.x = 0
player.y = 0
player.jump = 0
player.fall = 0
player.start_x = 0
player.start_y = 0
player.lives = 3
paused = false
tx = 0
ty = 0
fall = false
up_down = false

function kolo.load()
	player.speed = player.texture/8
	texture = player.texture
	global_bg = love.graphics.newImage("maps/"..tonumber(chapter).."/bg.png")
	global_bg:setWrap("repeat", "repeat")
	global_bg_quad = love.graphics.newQuad(0, 0, width, height, global_bg:getWidth(), global_bg:getHeight())
	map = require("maps/"..tonumber(chapter).."/"..tonumber(mise))
	-- TODO prepsat
	image = love.graphics.newImage("maps/"..chapter.."/images.png")
	player.image = love.graphics.newImage("graphics/images/postava.png")
	--generate images
	local id = 0
	local spacing = map.tilesets[1].spacing
	for i=0,(map.tilesets[1].imagewidth-map.tilesets[1].tilewidth),map.tilesets[1].tilewidth + spacing do
		for k=0,(map.tilesets[1].imageheight-map.tilesets[1].tileheight),map.tilesets[1].tileheight + spacing do
			id = id + 1
			segment[id] = love.graphics.newQuad(k, i, map.tilesets[1].tilewidth, map.tilesets[1].tileheight, map.tilesets[1].imagewidth, map.tilesets[1].imageheight)
		end
	end
	-- get start point
	for key, value in pairs(map.tilesets[1].tiles) do
		if value.properties["data"] then
			properties[value.id+1] = value.properties["data"]
		end
		if value.properties["color"] then
			procolor[value.id+1] = value.properties["color"]
		end
	end
	-- generate map
	local id = 0
	for i=0,map.height-1,1 do
		level[i] = {}
		level_bg[i] = {}
		for k=0,map.width-1,1 do
			id = id + 1
			level_bg[i][k] = map.layers[1].data[id]
			level[i][k] = map.layers[2].data[id]
			if properties[level[i][k]] == "start" then
				player.y = i*texture
				player.x = k*texture
				player.start_x = k*texture
				player.start_y = i*texture
			end
		end
	end
end

function kolo.konec()
	mise = mise + 1
	if mise == 6 then
		mise = 1
		chapter = chapter + 1
	end
	if (mise > tonumber(player.mise) and chapter == tonumber(player.chapter))
	    or chapter > tonumber(player.chapter) then
		player.chapter = chapter
		player.mise = mise
		love.filesystem.write("player.chapter", player.chapter)
		love.filesystem.write("player.mise", player.mise)
	end
	if chapter > 5 then
		mise = nil
		state = 2
	end
	keys = {}
	segment = {}
	level = {}
	level_bg = {}
	map_x = 0
	map_y = 0
	properties = {}
	procolor = {}
	map_color = {}
	player.key = {}
	player.x = 0
	player.y = 0
	player.jump = 0
	player.fall = 0
	player.start_x = 0
	player.start_y = 0
	player.lives = 3
	paused = false
	tx = 0
	ty = 0
	fall = false
	up_down = false
	love.filesystem.write("player.points", player.points)
	kolo.load()
end

function kolo.reset_player()
	effect["lose"]:play()
	player.x = player.start_x
	player.y = player.start_y
	player.fall = 0
end

function kolo.update(dt)
	if update then
		tlacitka.clear()
	end
	if love.system.getOS() == "Android" then
		if not love.graphics.isActive( ) then
			paused = true
			music[3]:stop()
		else
			paused = false
			music[3]:play()
		end
		if update then
			tlacitka.clear()
			tlacitka.spawn(10, height -((200*tlacitka.scale)), language.left, "left")
			tlacitka.spawn(width - (10+(200*tlacitka.scale)), height -((200*tlacitka.scale)), language.right, "right")
			tlacitka.spawn(10, height -((200*tlacitka.scale)*2), language.up, "up")
			tlacitka.spawn(width - (10+(200*tlacitka.scale)), height -((200*tlacitka.scale)*2), language.down, "down")
			tlacitka.spawn((10+(200*tlacitka.scale)), height -((200*tlacitka.scale)), language.jump, "jump")
			tlacitka.spawn(width - (10+(200*tlacitka.scale)*2), height -((200*tlacitka.scale)), language.jump, "jump")
		end
		tlacitka.check()
	end
	global_bg_quad = love.graphics.newQuad(0, 0, width, height, global_bg:getWidth(), global_bg:getHeight())
	if not paused then
		left = true
		right = true
		up_down = false
		local i_tmp = player.x/texture
		local k_tmp = player.y/texture
		local i = round(i_tmp)
		local k = round(k_tmp)
		if i < 0 or i > map.width or k > map.height-2 then
			kolo.reset_player()
		end
		fall = true
		if k > -1 and i > -1 and k < map.height and i < map.width and level[k][i] ~= nil then
			if properties[level[round(k_tmp+0.5)][i]] == "ground" or
			( properties[level[round(k_tmp+0.5)][i]] == "lock"
			and not check_color(procolor[level[round(k_tmp+0.5)][i]])) then
				fall = false
				player.fall = 0
				player.y = (round(k_tmp-0.5))*texture
			end
			if k > 0 then
				if properties[level[round(k_tmp-0.5)][i]] == "ground" or
				( properties[level[round(k_tmp-0.5)][i]] == "lock"
				and not check_color(procolor[level[round(k_tmp-0.5)][i]])) then
					player.jump = 0
				end
			end
			if properties[level[k][round(i_tmp-0.5)]] == "ground" or
			( properties[level[k][round(i_tmp-0.5)]] == "lock"
			and not check_color(procolor[level[k][round(i_tmp-0.5)]])) then
				left = false
				--player.x = (round(i_tmp+0.5))*texture-1
			end
			if properties[level[k][round(i_tmp+0.5)]] == "ground" or
			( properties[level[k][round(i_tmp+0.5)]] == "lock"
			and not check_color(procolor[level[k][round(i_tmp+0.5)]])) then
				right = false
				--player.x = (round(i_tmp-0.5))*texture+1
			end
			if level[k][i] > 0 then
				if properties[level[k][i]] == "most" then
					fall = false
					player.fall = 0
					player.y = (k*texture)-15
				end
				if properties[level[k][i]] == "spikes" then
					kolo.reset_player()
				end
				if properties[level[k][i]] == "coin" then
					level[k][i] = 0
					player.points = player.points + 1
					effect["pick"]:play()
				end
				if properties[level[k][i]] == "krystal" then
					level[k][i] = 0
					player.points = player.points + 5
					effect["pick"]:play()
				end
				if properties[level[k][i]] == "hvezda" then
					level[k][i] = 0
					player.points = player.points + 10
					effect["pick"]:play()
				end
				if properties[level[k][i]] == "key" then
					player.key[level[k][i]] = procolor[level[k][i]]
					table.insert(keys, tonumber(level[k][i]))

					level[k][i] = 0
					effect["pick2"]:play()
				end
				if properties[level[k][i]] == "lader" or properties[level[round(k_tmp+0.5)][i]] == "lader" then
					up_down = true
					player.fall = 0
					fall = false
				end
				if properties[level[k][i]] == "jump" then
					player.jump = math.ceil(player.texture/1.6)
					player.fall = 0
					effect["jump"]:play()
				end
				if properties[level[k][i]] == "end" then
					effect["win"]:play()
					kolo.konec()
				end
			end
		end
		if player.jump < 0 then
			player.jump = 0
		end
		if fall and player.jump == 0 then
			player.fall = player.fall + (player.texture/48)
		end
		if player.fall > 0 then
			player.y = player.y + player.fall
		end
		if player.jump > 0 then
			player.y = player.y - player.jump
			player.jump = player.jump - (player.texture/48)
		end
		if(love.keyboard.isDown('left')) and left  then
			player.x = player.x - player.speed
		end
		if(love.keyboard.isDown('right')) and right then
			player.x = player.x + player.speed
		end
		if up_down then
			if(love.keyboard.isDown('up')) then
				player.y = player.y - player.speed
			end
			if(love.keyboard.isDown('down')) then
				player.y = player.y + player.speed
			end
		end
		if love.system.getOS() == "Android" then
			local tmp = tlacitka.pres_table()
			for k, v in pairs(tmp) do
				press = v
				if press == "left" and left  then
					player.x = player.x - player.speed
				end
				if press == "right" and right then
					player.x = player.x + player.speed
				end
				if press == "jump" then
					if not fall then
						player.jump = player.texture/2.4
						effect["jump"]:play()
					end
				end
				if up_down then
					if press == "up" then
						player.y = player.y - player.speed
					end
					if press == "down" then
						player.y = player.y + player.speed
					end
				end
			end
		end
	end
end

function check_color(color)
	for key, value in pairs(player.key) do
		if value == color then
			return true
		end
	end
	return false
end

function kolo.mousepressed(x, y, button)
	if button == "l" then
      tlacitka.click(x, y)
  end
end

function kolo.keypressed(key)
	if key == " " or key == "up" then
		if not fall then
			player.jump = math.ceil(player.texture/2.4)
			effect["jump"]:play()
		end
	end
	if key == "p" then
		paused = not paused
	end
	if key == "escape" then
		--chapter = nil
		mise = nil
		state = 2
	end
end

function kolo.draw()
	love.graphics.setColor(255, 255, 255)
	love.graphics.draw(global_bg, global_bg_quad, 0, 0)
	tx = math.floor(-(player.x-width/2)+0.5)
	ty = math.floor(-(player.y-height/2)+0.5)
	if tx > 0 then
		tx = 0
	end
	if ty > 0 then
		ty = 0
	end
	if ty < -((map.height*texture)-height) then
		ty = -((map.height*texture)-height)
	end
	if tx < -((map.width*texture)-width) then
		tx = -((map.width*texture)-width)
	end
	for i=-(math.floor(ty/texture)+1),-(math.ceil((ty-height)/texture)-1),1 do
		for k=-(math.floor(tx/texture)+1),-(math.ceil((tx-width)/texture)-1),1 do
			if i > -1 and k > -1 and i < map.height and k < map.width then
				if level_bg[i][k] ~= nil and  level_bg[i][k] > 0 then
						love.graphics.draw(image,segment[level_bg[i][k]],tx+(k*texture),ty+(i*texture),0,(texture/70),(texture/70))
				end
				if level[i][k] ~= nil and level[i][k] > 0 then
					if level[i][k] ~= 1610612752 then
						love.graphics.draw(image,segment[level[i][k]],tx+(k*texture),ty+(i*texture),0,(texture/70),(texture/70))
					end
				end
			end
		end
	end
	love.graphics.draw(player.image,tx+player.x,ty+player.y,0,(texture/70),(texture/70))
	love.graphics.setColor(0, 0, 255)
	love.graphics.print(map.layers[2].name,10,10)
	love.graphics.print(language.points..player.points, (width-menuf:getWidth(language.points..tostring(player.points))) - 10, 10)
	love.graphics.setColor(255, 255, 255)
	for key, value in pairs(keys) do
		love.graphics.draw(image, segment[value], 20*(key+1), 30)
	end
	if paused then
		love.graphics.setColor(255, 0, 0)
		love.graphics.print(language.pause, width/2 - menuf:getWidth(language.pause) / 2, height/2 - menuf:getHeight() / 2)
	end
	--if love.system.getOS() == "Android" then
		tlacitka.draw()
	--end
end

return kolo
