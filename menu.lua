local menu = {}
local tlacitka = require("buttons")
local bg = love.graphics.newImage("graphics/bg/darkPurple.png")
bg:setWrap("repeat", "repeat")
local credits1 = love.graphics.newImage("credits.png")
local credits2 = love.graphics.newImage("kenney.png")

function menu.update()
  quad = love.graphics.newQuad(0, 0, width, height, 256, 256)
  if update then
      tlacitka.clear()
      tlacitka.spawn(width/2 - (10+(200*tlacitka.scale)/2), height/2 -((50*tlacitka.scale)*2), language.missions, 2)
      tlacitka.spawn(width/2 - (10+(200*tlacitka.scale)/2), height/2 - ((30*tlacitka.scale)), language.extra, 4)
      tlacitka.spawn(width/2 - (10+(200*tlacitka.scale)/2), height/2 +((30*tlacitka.scale)), language.setting, 5)
      tlacitka.spawn(width/2 - (10+(200*tlacitka.scale)/2), height/2 +((50*tlacitka.scale)*2), language.quit, 6)
      update = false
  end
  tlacitka.check()
  local push = tlacitka.presset()
  if  push ~= nil then
      state = push
  end
end

function menu.mousepressed(x, y, button)
  if button == "l" then
    if tlacitka.click(x, y) then
      tlacitka.click(x, y)
    end
  end
end

function menu.keypressed(key)
    tlacitka.keyboard(key)
end

function menu.draw()
  love.graphics.draw(bg, quad, 0, 0)
  love.graphics.draw(credits1, 10, (height-credits1:getHeight())-10)
  love.graphics.draw(credits2, (width-credits2:getWidth())-10, (height-credits2:getHeight())-10)
  tlacitka.draw()
end

return menu
